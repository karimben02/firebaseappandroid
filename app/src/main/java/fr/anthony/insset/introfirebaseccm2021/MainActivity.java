package fr.anthony.insset.introfirebaseccm2021;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Document;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MainActivity extends AppCompatActivity {

    private FirebaseDatabase maBase;
    private ValueEventListener monListener;
    private Pojo simplepojo;
    private Button button_lire_pojo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button_lire_pojo =findViewById(R.id.button_lire_pojo);
       /* button_lire_pojo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reactionLirePojo();
            }
        });*/
        button_lire_pojo.setOnClickListener(v -> reactionLirePojo());


        maBase = FirebaseDatabase.getInstance("https://dbfirebaseandroid-default-rtdb.europe-west1.firebasedatabase.app/");
        monListener = null;
    }

    private void reactionLirePojo() {
        simplepojo = null;
        Task<DataSnapshot> taskdatasnap = maBase.getReference("4").get();

        taskdatasnap
                .addOnSuccessListener(
                        dataSnapshot -> {
                            Log.d("Anthony", "SUCCESS");
                            simplepojo=dataSnapshot.getValue(Pojo.class);

                            Toast.makeText(MainActivity.this,"Pojo reçu:"+ simplepojo, Toast.LENGTH_LONG).show();
                        }
                )
                .addOnFailureListener(ex -> {
                    Log.d("Anthony", "Failure: " + ex);
                } )
                .addOnCanceledListener(() -> {
                    Log.d("Anthony", "CANCEL");
                });
    }

    public void onClickPremierAjout(View view) {
        maBase.getReference("First Try").setValue("Bonjour");

        List<String> desChaine = IntStream.range(1,5).mapToObj(i->new String("chaine"+i)).collect(Collectors.toList());
        maBase.getReference("Second Try").setValue(desChaine);
        maBase.getReference("Third Try").setValue(12);
        Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show();
    }

    public void onClickAjouterListener(View view) {
        if(monListener ==  null)
        {
            monListener = maBase.getReference("First Try").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    Toast.makeText(MainActivity.this, "SUCCESS" + snapshot, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(MainActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void onClickRetirerListener(View view) {
        if (monListener != null)
        {
            maBase.getReference("First Try").removeEventListener(monListener);
            monListener = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (monListener!=null) {
            maBase.getReference("First Try").removeEventListener(monListener);
            monListener=null;
        }
    }


    public void onClickAjouterPojo(View view) {
        if (simplepojo==null) {
            simplepojo = new Pojo(124,"4");

        maBase.getReference("4").setValue(simplepojo);
        }
    }

    public void onClickPassageOffline(View view) {
        maBase.goOffline();
        Log.d("Anthony", "Offline");
    }

    private static int compteur = 0;
    public void onClickAjouterDonnee(View view) {
        maBase.getReference("Entree"+compteur).setValue(Integer.valueOf(100* compteur));
        compteur ++;
    }

    public void onClickReourOnline(View view) {
        maBase.goOnline();
        Log.d("Anthony", "On vient de passer en Offline");
    }
}
