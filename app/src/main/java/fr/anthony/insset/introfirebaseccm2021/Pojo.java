package fr.anthony.insset.introfirebaseccm2021;

public class Pojo {
    private int donnee1;
    private String donne2;

    public Pojo() {
        this(0, "Pardef");
    }
    public Pojo(int donnee1, String donne2) {
        this.donnee1 = donnee1;
        this.donne2 = donne2;
    }

    @Override
    public String toString() {
        return "Pojo{" +
                "donnee1=" + donnee1 +
                ", donne2='" + donne2 + '\'' +
                '}';
    }

    public int getDonnee1() {
        return donnee1;
    }

    public void setDonnee1(int donnee1) {
        this.donnee1 = donnee1;
    }

    public String getDonne2() {
        return donne2;
    }

    public void setDonne2(String donne2) {
        this.donne2 = donne2;
    }
}
